
#include "includes.h"
#include "defs.h"

extern int handler(int );
int writ=0;
char *outfilename = NULL;
int writesize = 0;
char *output = NULL;
void sighandler();
void print_help();

int launch_dummy()
{
	return execve(dummy,NULL,NULL);
}

int main(int argc,char **argv)
{
	const char *optstring = "hm:o:";
	if(argc < 2) {
		printf("Usage: %s [options]\n",argv[0]);
		return -1;
	}
	struct handle *h = (struct handle*)malloc(sizeof(struct handle));
	int nextopt=0;
	while(nextopt != -1){
		nextopt = getopt_long(argc,argv,optstring,NULL,NULL);
		switch(nextopt){
			case 'h':
				print_help();
				return -1;
			case 'm':
				h->mode = atoi(optarg);
				if(h->mode == 1)
					writ = 1;
				break;
			case 'o':
				h->outfile = optarg;
				outfilename = h->outfile;
				break;
			case '?':
				print_help();
				exit(0);
				break;
			case -1:
				break;
		}
	}
	signal(SIGINT,&sighandler);
	h->pid = 0;
	if(h->mode == 1 && h->outfile == NULL){
		printf(BOLD_RED "live mode requires outfile\n");
		exit(0);
	}
	if((h->pid = fork()) != 0){
		interactive(h);
	} else {
		if(ptrace(PTRACE_TRACEME,h->pid,NULL,NULL) == -1) 
			printf("ERROR PTRACE_TRACEME");
		launch_dummy();
	}
	return 0;
}

void interactive(struct handle *h)
{
	h->code = (char*)malloc(0x1000);//buffer for holding input
	h->encode =NULL;
	uint64_t addr = 0x401000;
	h->count=0;
	h->size = 0;
	h->ks = NULL;
	int status;
	h->pos = (char*)addr;
	if(ks_open(KS_ARCH_X86,KS_MODE_64,&h->ks) != KS_ERR_OK) {
		printf(BOLD_RED"Ks open error\n");
		return;
	}
	waitpid(h->pid,&status,0);
	if(ptrace(PTRACE_GETREGS,h->pid,NULL,&h->regs) == -1) 
		printf(BOLD_RED"ERROR PTRACE_GETREGS");
	//printf("%p\n",regs.rip);
	if(ptrace(PTRACE_SINGLESTEP,h->pid,NULL,NULL) == -1) 
		printf(BOLD_RED"ERROR PTRACE_SINGLESTEP");
	waitpid(h->pid,&status,0);
	printregs(&h->regs);
	printstack(h->regs.rsp,h->pid);
	if(h->mode == 1) {
		h->outputbuffer = (char *)malloc(0x1000);
		output = h->outputbuffer;
	}
	while(1){	
		printf("\n"BOLD_GREEN">>>   ");
		memset(h->code,0,0x1000);
		printf(BOLD_WHITE);
		fgets(h->code,0x500,stdin);
		//printf("\033[2J");
		assembler(h);	
	}
	return 0;
}

void printregs(struct user_regs_struct *regs)
{
	printf(BOLD_RED"\nREGS:\n\n");
	printf(BOLD_YELLOW "RAX : "WHITE"0x%-10"x64"\n"\
			BOLD_YELLOW "RBX : "WHITE"0x%-10"x64"\n"\
			BOLD_YELLOW "RCX : "WHITE"0x%-10"x64"\n"\
			BOLD_YELLOW "RDX : "WHITE"0x%-10"x64"\n"\
			BOLD_YELLOW "RDI : "WHITE"0x%-10"x64"\n"\
			BOLD_YELLOW "RSI : "WHITE"0x%-10"x64"\n"\
			BOLD_YELLOW "RBP : "WHITE"0x%-10"x64"\n"\
			BOLD_YELLOW "RSP : "WHITE"0x%-10"x64"\n"\
			BOLD_YELLOW "R8  : "WHITE"0x%-10"x64"\n"\
			BOLD_YELLOW "R9  : "WHITE"0x%-10"x64"\n"\
			BOLD_YELLOW "R10 : "WHITE"0x%-10"x64"\n"\
			BOLD_YELLOW "R11 : "WHITE"0x%-10"x64"\n"\
			BOLD_YELLOW "R12 : "WHITE"0x%-10"x64"\n"\
			BOLD_YELLOW "R13 : "WHITE"0x%-10"x64"\n"\
			BOLD_YELLOW "R14 : "WHITE"0x%-10"x64"\n"\
			BOLD_YELLOW "R15 : "WHITE"0x%-10"x64"\n"\
			BOLD_YELLOW "RIP : "WHITE"0x%-10"x64"\n",\
			regs->rax,regs->rbx,regs->rcx,regs->rdx,\
			regs->rdi,regs->rsi,regs->rbp,regs->rsp,regs->r8,regs->r9,\
			regs->r10,regs->r11,regs->r12,regs->r13,regs->r14,regs->r15,regs->rip);
	printf(WHITE);
	return;
}

void printstack(uint64_t rsp,int pid)
{
	uint64_t size = 240;
	uint64_t rtemp = rsp;
	char * stack = (char*)malloc(0x200);
	char *temp = stack;
	char *rest = stack;
	printf(BOLD_YELLOW "\nSTACK:\n\n");
	for(int i=0;i<size/2;i++){
		uint16_t data = ptrace(PTRACE_PEEKDATA,pid,rtemp,NULL);
		*(uint16_t*)temp = data;
		temp += 2;
		rtemp += 2;
	}
	int poi = 0;
	for(int i=0;i<size/16;i++){
		printf(BOLD_BLUE "0x%"x64": ",rsp);
		for(int j=0;j<16;j++)
			printf(WHITE "%02x ",*(unsigned char*)stack++);	
		poi += 16;
		rsp += 16;
		stack += 16;
		printf("\n");
	}
	memset(rest,0,size);
	free(rest);
	return;
}

void printflags(uint64_t eflags)
{
	printf(BOLD_GREEN "\nFLAGS\n");
	printf(BOLD_BLUE "CF : %d PF : %d AF : %d "\
			"ZF : %d SF : %d TF : %d "\
			"IF : %d DF : %d OF : %d \n\n",FLAG(eflags,_CF_MASK),\
			FLAG(eflags,_PF_MASK),FLAG(eflags,_AF_MASK),FLAG(eflags,_ZF_MASK),\
			FLAG(eflags,_SF_MASK),FLAG(eflags,_TF_MASK),FLAG(eflags,_IF_MASK),\
			FLAG(eflags,_DF_MASK),FLAG(eflags,_OF_MASK));
	printf(WHITE);
}

void assembler(struct handle *h)
{
	int status;
	
	if(ks_asm(h->ks,h->code,0,&h->encode,&h->size,&h->count) == KS_ERR_OK){
		char *temp = h->encode;
		char test[2];
		int i=h->size;
		writesize += h->size;
		if(h->mode == 1){
			memcpy(h->outputbuffer,h->encode,h->size);
			h->outputbuffer += h->size;
		}
		//printf("Size:%d\n",size);
		//printf("Count:%d\n",count);
		while(i>0){
			if(h->size == 1){
				temp[1] = '\x90';
			}
			uint16_t data = *(uint16_t*)temp;
			if(ptrace(PTRACE_POKETEXT,h->pid,h->pos,data) == -1){
				printf("PTRACE_POKETEXT");
				fflush(stdout);
			}
			temp+=2;
			h->pos += 2;
			if(i == 1){
				i-=1;
				continue;
			}
			i-=2;
		}
		ptrace(PTRACE_GETREGS,h->pid,NULL,&h->regs);
		//printf("RIP at:%p\n",regs.rip);
		//regs.rip = addr;
		//ptrace(PTRACE_SETREGS,pid,NULL,&regs);
		ptrace(PTRACE_SINGLESTEP,h->pid,NULL,NULL);
		waitpid(h->pid,&status,0);	
		ptrace(PTRACE_GETREGS,h->pid,NULL,&h->regs);
		printf("\n%s\n",h->code);
		printregs(&h->regs);
		printflags((uint64_t)h->regs.eflags);
		printstack((uint64_t)h->regs.rsp,h->pid);
		if(!WIFEXITED(status)){
			if(WIFSTOPPED(status))
				handler(status);
		}
		h->pos = (char*)h->regs.rip;
	} else {
		printf("Invalid Instruction\n");
	}
}
void print_help()
{
	printf(BOLD_YELLOW "Interactive Assembler:\n");
	printf(WHITE "\t-h - view this help\n");
	printf("\t-m - specifes the experiment mode(0) or live mode(1)\n"\
			"\t\texpermiment mode - Allows to execute code without saving\n"\
			"\t\tlive mode - Write the code assembled code into output file\n");
	printf("\t-o - output filename \n");
	return;
}

void sighandler()
{
	if(writ){
		int fd;
		if((fd=open(outfilename,O_WRONLY|O_CREAT|O_TRUNC)) < 0) {
			printf(BOLD_RED "Unable to open output file for writing\n");
			perror("open error");
			exit(0);
		}
		write(fd,output,writesize);
		close(fd);
		exit(0);
	}
	exit(0);
	return;
}
