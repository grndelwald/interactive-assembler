### Interactive Assembler

A simple run time assembler for intel **x86_64** bit assembly language for linux. It will be very much helpfull in learning assembly language and shellcoding. Just clone the repository, **change the permissions on the temp and interactive_assembler as executable***. Run interactive_assembler.

### Running

git clone https://gitlab.com/grndelwald/interactive-assembler.git

cd interactive-assembler

chmod 777 temp

chmod 777 interactive_assembler

./interactive_assembler -h


### Depenendcies

[Keystone Assembler](https://github.com/keystone-engine/keystone)

### Limitation:

For now, it can support only for linux and x86 architecture. 

### Screenshots

[![UI representation](/images/image2.png "Hello world")]




