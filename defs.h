#define x64 PRIx64
#define FLAG(eflag,MASK) ((eflag) & MASK ? 1: 0)
#define dummy "temp"
#define _CF_MASK 0x1
#define _PF_MASK 0x4
#define _AF_MASK 0x10
#define _ZF_MASK 0x40
#define _SF_MASK 0x80
#define _TF_MASK 0x100
#define _IF_MASK 0x200
#define _DF_MASK 0x400
#define _OF_MASK 0x800

#define BOLD_GREEN "\033[1;32m"
#define BOLD_YELLOW "\033[1;33m"
#define BOLD_RED "\033[1;31m"
#define BOLD_BLUE "\033[1;34m"
#define BLACK 	"\033[0;30m"
#define WHITE	"\033[0;37m"
#define BOLD_WHITE "\033[1;37m"

struct handle{
	int pid;
	unsigned char *code;
	unsigned char *encode;
	size_t count;
	size_t size;
	ks_engine *ks;
	char *pos;
	struct user_regs_struct regs;
	int mode;
	char *outfile;
	char *outputbuffer;
};

void interactive(struct handle *);
void printregs(struct user_regs_struct * );
void printstack(uint64_t,int);
void printflags(uint64_t);
void assembler(struct handle *);

