#include "includes.h"
#include "defs.h"
int handler(int status)
{
	switch(WSTOPSIG(status)){
		case SIGINT:
			printf(BOLD_RED "INTERRUPTED\n");
			break;
		case SIGILL:
			printf(BOLD_RED "Illegal instrcution\n");
			break;
		case SIGSEGV:
			printf(BOLD_RED "Segmentation fault\n");
			break;
		case SIGSYS:
			printf(BOLD_RED "Bad argument to syscall\n");
			break;
	}
	return 0;
}
